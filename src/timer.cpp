#pragma once

#include <chrono>

class Timer {
private:
    std::chrono::steady_clock::time_point last_step;

public:
    Timer() {
        last_step = std::chrono::steady_clock::now();
    }

    float step() {
        std::chrono::steady_clock::time_point current_step = std::chrono::steady_clock::now();
        float step_duration = std::chrono::duration_cast<std::chrono::milliseconds> (current_step - last_step).count();
        last_step = current_step;
        return step_duration;
    }
};