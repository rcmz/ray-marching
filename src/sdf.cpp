#pragma once

#include "includes.cpp"
#include "mesh.cpp"
#include "bounds.cpp"
#include "omgl.cpp"
#include "timer.cpp"

class SDF {
private:
    ivec3 size;
    mat4 transform;
    omgl::texture distanceTexture;
    omgl::texture signTexture;
    omgl::texture sdfTexture;

public:
    SDF(const Mesh& mesh, int width) {
        Bounds mesh_bounds(mesh.getVertices());
        mesh_bounds.pad(1);
        mesh_bounds.cubify();
        size = ivec3(width);
        float resolution = mesh_bounds.size().x / width;
        transform = translate(mesh_bounds.min()) * scale(vec3(size) * resolution);

        const vector<vec3> vertices = mesh.getVertices();
        uint verticesBuffer;
        glGenBuffers(1, &verticesBuffer);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, verticesBuffer);
        glBufferData(GL_SHADER_STORAGE_BUFFER, vertices.size()*sizeof(vec3), vertices.data(), GL_STATIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, verticesBuffer);

        Timer timer;

        // distance

        distanceTexture = omgl::texture(GL_R32F, size);
        distanceTexture.filter(GL_LINEAR);
        distanceTexture.image(0);

        omgl::program distanceProgram({ "shaders/distance.comp" });
        distanceProgram.uniform("sdfMat", transform);

        for (int z = 0; z < size.z; z++) {
            cout << "\rcomputing distance layer : " << z + 1 << "/" << size.z << flush;
            distanceProgram.uniform("workGroupIDZ", z);
            glDispatchCompute(size.x, size.y, 1);
            glFinish();
        }

        cout << " took " << timer.step() << " ms" << endl;

        // sign

        signTexture = omgl::texture(GL_R32F, size);
        signTexture.filter(GL_LINEAR);
        signTexture.image(1);

        omgl::program signProgram({ "shaders/sign.comp" });
        signProgram.uniform("sdfMat", transform);

        for (int z = 0; z < size.z; z++) {
            cout << "\rcomputing sign layer : " << z + 1 << "/" << size.z << flush;
            signProgram.uniform("workGroupIDZ", z);
            glDispatchCompute(size.x, size.y, 1);
            glFinish();
        }

        cout << " took " << timer.step() << " ms" << endl;

        // sdf

        sdfTexture = omgl::texture(GL_R32F, size);
        sdfTexture.filter(GL_LINEAR);
        sdfTexture.image(2);

        omgl::program sdfProgram({ "shaders/sdf.comp" });
        sdfProgram.uniform("sdfMat", transform);
        glDispatchCompute(size.x, size.y, size.z);
    }

    void save(string path) const {
        vector<float> field = vector<float>(size.x * size.y * size.z);
        sdfTexture.bind();
        glGetTexImage(GL_TEXTURE_3D, 0, GL_RED, GL_FLOAT, field.data());
        glBindTexture(GL_TEXTURE_3D, 0);

        ofstream fileStream(path);

        if (fileStream.is_open()) {
            for (int z = 0; z < size.z; z++) {
                for (int y = 0; y < size.y; y++) {
                    for (int x = 0; x < size.x; x++) {
                        int index = x + size.x*(y + size.y*z);
                        fileStream << field[index];

                        if (x != size.x - 1) fileStream << "\t";
                    }

                    if (y != size.y - 1) fileStream << "\n";
                }

                if (z != size.z - 1) fileStream << "\n\n";
            }

            fileStream.close();
        } else {
            cout << "Impossible to open " << path << endl;
        }
    }

    const ivec3& getSize() const {
        return size;
    }

    const mat4& getTransform() const {
        return transform;
    }

    const omgl::texture& getDistanceTexture() const {
        return distanceTexture;
    }

    const omgl::texture& getSignTexture() const {
        return signTexture;
    }

    const omgl::texture& getSdfTexture() const {
        return sdfTexture;
    }
};