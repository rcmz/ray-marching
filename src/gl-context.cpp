#pragma once

#include "includes.cpp"

void debugCallback(
    uint source, 
    uint type, 
    uint id, 
    uint severity, 
    int length, 
    const char* message, 
    const void* userParam)
{
    string source_tag = "UNKNOWN";
    string type_tag = "UNKNOWN";
    string severity_tag = "UNKNOWN";

    switch (source) {
        case GL_DEBUG_SOURCE_API: source_tag = "API"; break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM: source_tag = "WINDOW_SYSTEM"; break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER: source_tag = "SHADER_COMPILER"; break;
        case GL_DEBUG_SOURCE_THIRD_PARTY: source_tag = "THIRD_PARTY"; break;
        case GL_DEBUG_SOURCE_APPLICATION: source_tag = "APPLICATION"; break;
        case GL_DEBUG_SOURCE_OTHER: source_tag = "OTHER"; break;
    }

    switch (type) {
        case GL_DEBUG_TYPE_ERROR: type_tag = "ERROR"; break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: type_tag = "DEPRECATED_BEHAVIOR"; break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: type_tag = "UNDEFINED_BEHAVIOR"; break;
        case GL_DEBUG_TYPE_PORTABILITY: type_tag = "PORTABILITY"; break;
        case GL_DEBUG_TYPE_PERFORMANCE: type_tag = "PERFORMANCE"; break;
        case GL_DEBUG_TYPE_MARKER: type_tag = "MARKER"; break;
        case GL_DEBUG_TYPE_PUSH_GROUP: type_tag = "PUSH_GROUP"; break;
        case GL_DEBUG_TYPE_POP_GROUP: type_tag = "POP_GROUP"; break;
        case GL_DEBUG_TYPE_OTHER: type_tag = "OTHER"; break;
    }

    switch (severity) {
        case GL_DEBUG_SEVERITY_HIGH: severity_tag = "HIGH"; break;
        case GL_DEBUG_SEVERITY_MEDIUM: severity_tag = "MEDIUM"; break;
        case GL_DEBUG_SEVERITY_LOW: severity_tag = "LOW"; break;
        case GL_DEBUG_SEVERITY_NOTIFICATION: severity_tag = "NOTIFICATION"; break;
    }

    if (severity != GL_DEBUG_SEVERITY_NOTIFICATION) {
        cout << source_tag << " " << type_tag << " " << severity_tag << " " << message << endl;
    }
}

GLFWwindow* initGL(uint width, uint height, string title) {
    if (glfwInit() == false) {
        cout << "Failed to initialize GLFW" << endl;
        exit(1);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

    GLFWwindow* window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);

    if (window == nullptr) {
        cout << "Failed to open GLFW window" << endl;
        exit(1);
    }

    glfwSetInputMode(window, GLFW_STICKY_MOUSE_BUTTONS, true);
    glfwSetInputMode(window, GLFW_STICKY_KEYS, true);

    glfwMakeContextCurrent(window);

    if (glewInit() != GLEW_OK) {
        cout << "Failed to initialize GLEW" << endl;
        exit(1);
    }

    glDebugMessageCallback(debugCallback, nullptr);

    return window;
}

void terminateGL() {
    glfwTerminate();
}

