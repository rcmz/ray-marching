#pragma once

#include <vector>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

class Bounds {
private:
    glm::vec3 _min;
    glm::vec3 _max;
    
public:
    Bounds(const std::vector<glm::vec3>& vertices) {
        _min = glm::vec3(std::numeric_limits<float>::max());
        _max = glm::vec3(std::numeric_limits<float>::min());

        for (glm::vec3 vertex : vertices) {
            _min = glm::min(_min, vertex);
            _max = glm::max(_max, vertex);
        }
    }

    void pad(float padding) {
        _min -= padding;
        _max += padding;
    }

    void cubify() {
        float max_axis = glm::max(size().x, size().y, size().z);
        _min = center() - glm::vec3(max_axis) / 2.0f;
        _max = center() + glm::vec3(max_axis) / 2.0f;
    }

    const glm::vec3& min() const {
        return _min;
    }

    const glm::vec3& max() const {
        return _max;
    }

    glm::vec3 size() const {
        return _max - _min;
    }

    glm::vec3 center() const {
        return (_min + _max) / 2.0f;
    }
};