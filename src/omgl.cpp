#pragma once

#include <stdlib.h>
#include <stdio.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cstring>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/transform.hpp>

namespace omgl {

using namespace std;
using namespace glm;

string file_to_str(const string& path) {
	string result;
	ifstream file_stream(path);

	if(file_stream.is_open()){
		stringstream string_stream;
		string_stream << file_stream.rdbuf();
		result = string_stream.str();
		file_stream.close();
	}else{
        cerr << "Impossible to open : " << path << endl;
	}

    return result;
}

bool str_ends_with(const string& s, const string& e) {
	int p = s.size() - e.size();

	if (p < 0) {
		cerr << s << " can not ends with " << e << endl;
		return false;
	}

	for (int i = 0; i < e.size(); i++) {
		if (s[p + i] != e[i]) {
			return false;
		}
	}

	return true;
}

class program {
private:
    uint id;

    uint create_shader(const string& path) {
        uint type = 0;

             if (str_ends_with(path, ".vert")) type = GL_VERTEX_SHADER;
        else if (str_ends_with(path, ".frag")) type = GL_FRAGMENT_SHADER;
        else if (str_ends_with(path, ".tesc")) type = GL_TESS_CONTROL_SHADER;
        else if (str_ends_with(path, ".tese")) type = GL_TESS_EVALUATION_SHADER;
        else if (str_ends_with(path, ".geom")) type = GL_GEOMETRY_SHADER;
        else if (str_ends_with(path, ".comp")) type = GL_COMPUTE_SHADER;
        else cerr << "Unknow shader type : " << path << endl;

        uint shader = glCreateShader(type);
        string source = file_to_str(path);
        const char* source_c_str = source.c_str();
        glShaderSource(shader, 1, &source_c_str, nullptr);
        glCompileShader(shader);
        return shader;
    }
public:
    uint get_id() {
        return id;
    }

    void use() {
        glUseProgram(id);
    }

    program(const vector<string>& shader_paths) {
        id = glCreateProgram();
        vector<uint> shaders;

        for (string shader_path : shader_paths) {
            uint shader = create_shader(shader_path);
            shaders.push_back(shader);
            glAttachShader(id, shader);
        }

        glLinkProgram(id);

        for (uint shader : shaders) {
            glDetachShader(id, shader);
            glDeleteShader(shader);
        }

        use();
    }

    void uniform(const string& name, float value) {
        use();
        uint uniform_location = glGetUniformLocation(id, name.c_str());
        glUniform1f(uniform_location, value);
    }

    void uniform(const string& name, int value) {
        use();
        uint uniform_location = glGetUniformLocation(id, name.c_str());
        glUniform1i(uniform_location, value);
    }

    void uniform(const string& name, const ivec2& value) {
        use();
        uint uniform_location = glGetUniformLocation(id, name.c_str());
        glUniform2iv(uniform_location, 1, &value[0]);
    }

    void uniform(const string& name, const mat4& value) {
        use();
        uint uniform_location = glGetUniformLocation(id, name.c_str());
        glUniformMatrix4fv(uniform_location, 1, false, &value[0][0]);
    }
};

class texture {
private:
    uint id;
    uint target;
    uint format;
public:
    void bind() const {
        glBindTexture(target, id);
    }

    texture() {
    }

    texture(uint format, ivec3 size) {
        this->target = GL_TEXTURE_3D;
        this->format = format;

        glGenTextures(1, &id);
        bind();
        glTexStorage3D(target, 1, format, size.x, size.y, size.z);
    }

    void filter(uint filter) {
        bind();
        glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filter);
        glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filter);
    }

    void image(uint index) {
        glBindImageTexture(index, id, 0, true, 0, GL_READ_WRITE, format);
    }

    void unit(uint index) const {
        glActiveTexture(GL_TEXTURE0 + index);
        bind();
    }
};

}