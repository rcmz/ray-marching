#pragma once

#include "includes.cpp"
#include "utils.cpp"

class Mesh {
private:
    vector<vec3> vertices;

public:
    Mesh(const string& path) {
        ifstream fileStream(path);

        if (fileStream.is_open()) {
            if (strEndsWith(path, ".stl")) {
                string lineHeader;
                vec3 vertex;

                while (!fileStream.eof()) {
                    fileStream >> lineHeader;

                    if (lineHeader == "vertex") {
                        fileStream >> vertex.x >> vertex.y >> vertex.z;
                        vertices.push_back(vertex);
                    } else {
                        fileStream.ignore(numeric_limits<streamsize>::max(), '\n');
                    }
                }
            } else {
                cout << "Unrecognized file type : " << path << endl;
            }

            fileStream.close();
        } else {
            cout << "Impossible to open " << path << endl;
        }
    }

    const vector<vec3> getVertices() const {
        return vertices;
    }

    void computeBounds(vec3& min, vec3& max) const {
        min = vec3(numeric_limits<float>::max());
        max = vec3(numeric_limits<float>::min());

        for (vec3 vertex : vertices) {
            min = glm::min(min, vertex);
            max = glm::max(max, vertex);
        }
    }
};