#pragma once

#include "includes.cpp"

class Camera {
private:
    static Camera* activeCamera;

    GLFWwindow* window;
    vec3 pivot;
    float distance;
    float fov;

    vec2 rotAngles;
    vec2 previousMousePos;

    mat4 transform;

    static void scrollCallback(GLFWwindow* window, double scrollX, double scrollY) {
        activeCamera->distance = glm::max(activeCamera->distance - float(scrollY)/2.0f, 0.0f);
    }

    vec2 getMousePos() const {
        dvec2 mousePos;
        glfwGetCursorPos(window, &mousePos.x, &mousePos.y);
        return mousePos;
    }

    bool getMouseButton(int button) const {
        return glfwGetMouseButton(window, button);
    }

    void setMousePos(vec2 pos) const {
        glfwSetCursorPos(window, pos.x, pos.y);
    }

public:
    Camera(GLFWwindow* window) : 
        window(window), 
        pivot(0, 1, 0),
        distance(3),
        fov(60),
        rotAngles(0, 0)
    {
        activeCamera = this;
        glfwSetScrollCallback(window, scrollCallback);
    }

    void update() {
        glfwPollEvents();

        vec2 windowSize = getWindowSize();
        vec2 mousePos = getMousePos();
        vec2 mouseDelta = mousePos - previousMousePos;
        bool mouseButtonLeft = getMouseButton(GLFW_MOUSE_BUTTON_LEFT);
        bool mouseButtonRight = getMouseButton(GLFW_MOUSE_BUTTON_RIGHT);

        if (mouseButtonLeft) {
            rotAngles.x -= mouseDelta.y / 200;
            rotAngles.y -= mouseDelta.x / 200;
        }

        rotAngles.x = clamp(rotAngles.x, epsilon<float>() - pi<float>()/2, pi<float>()/2 + epsilon<float>());
        mat4 rot = rotate(rotAngles.y, vec3(0, 1, 0)) * rotate(rotAngles.x, vec3(1, 0, 0));

        vec3 right = rot * vec4(1, 0, 0, 0);
        vec3 up = rot * vec4(0, 1, 0, 0);
        // vec3 forward = rot * vec4(0, 0, 1, 0);

        if (mouseButtonRight) {
            pivot -= right * mouseDelta.x / 1000.0f * distance;
            pivot += up * mouseDelta.y / 1000.0f * distance;
        }

        if (mouseButtonLeft || mouseButtonRight) {
            if (mousePos.x < 0) mousePos.x += windowSize.x;
            if (mousePos.x > windowSize.x) mousePos.x -= windowSize.x;

            if (mousePos.y < 0) mousePos.y += windowSize.y;
            if (mousePos.y > windowSize.y) mousePos.y -= windowSize.y;

            setMousePos(mousePos);
        }

        previousMousePos = mousePos;

        vec3 pos = translate(pivot) * rot * translate(vec3(0, 0, distance)) * vec4(0, 0, 0, 1);
        transform = inverse(lookAt(pos, pivot, up));
    }

    const mat4& getTransform() const {
        return transform;
    }

    float getFov() const {
        return fov;
    }

    ivec2 getWindowSize() const {
        ivec2 windowSize;
        glfwGetFramebufferSize(window, &windowSize.x, &windowSize.y);
        return windowSize;
    }
};

Camera* Camera::activeCamera = nullptr;