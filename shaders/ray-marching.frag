#version 430

const int SAMPLE_PER_FRAGMENT = 16;
const int MAX_BOUNCE = 1;

struct Hit {
	bool hit;
	vec3 pos;
	float dist;	
	vec3 normal;
	vec3 color;
};

uniform sampler3D sdfSampler;
uniform ivec2 windowSize;
uniform mat4 sdfMat;
uniform mat4 cameraMat;
uniform float fov;

out vec3 fragColor;

const float FLOAT_MAX = 1000000;
float PI = radians(180);
vec2 fragCoord = gl_FragCoord.xy / windowSize;
uint fragID = uint(gl_FragCoord + gl_FragCoord.y*windowSize.x);
mat4 sdfMatInv = inverse(sdfMat);
ivec3 cellCount = textureSize(sdfSampler, 0);

float map(float value, float fromMin, float fromMax, float toMin, float toMax) {
	return (value - fromMin) / (fromMax - fromMin) * (toMax - toMin) + toMin;
}

vec2 rngCoord = fragCoord;

float randOneLiner() {
	float r = fract(sin(dot(rngCoord, vec2(12.9898,78.233))) * 43758.5453);
	rngCoord.x += r;
	return r;
}

uint wangHash(uint seed) {
	seed = (seed ^ uint(61)) ^ (seed >> uint(16));
    seed *= 9;
    seed = seed ^ (seed >> uint(4));
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> uint(15));
    return seed;
}

uint rngState = wangHash(fragID);

uint randXorShift() {
	rngState ^= (rngState << 13);
	rngState ^= (rngState >> 17);
	rngState ^= (rngState << 5);
	return rngState;
}

uint randLCG() {
	rngState = 1664525 * rngState + 1013904223;
	return rngState;
}

float rand() {
	return float(randXorShift()) / 4294967296.0;
}

vec3 sphericalRand() {
	// cubical rand seems to actualy be slower
	// return normalize(vec3(rand(), rand(), rand()));

	float theta = rand() * PI * 2;
	float phi = acos(rand()*2 - 1);

	float x = sin(phi) * cos(theta);
	float y = sin(phi) * sin(theta);
	float z = cos(phi);

	return vec3(x, y, z);
}

float distanceAt(vec3 point) {
	vec3 cell = vec3(sdfMatInv * vec4(point, 1));

	if (all(greaterThanEqual(cell, vec3(0))) && all(lessThan(cell, vec3(1)))) {
		return texture(sdfSampler, cell).r;
	} else {
		point = vec3(sdfMatInv * vec4(point, 1));
		vec3 v = max(max(vec3(0) - point, point - vec3(1)), vec3(0));
		float d = length(vec3(sdfMat * vec4(v, 0)));
		return d + 1; // + 1 to actualy go inside the sdf
	}
}

vec3 gradientAt(vec3 point) {
	const float h = 0.001;

	vec3 hx = vec3(1, 0, 0) * h;
	vec3 hy = vec3(0, 1, 0) * h;
	vec3 hz = vec3(0, 0, 1) * h;

	float dx = (distanceAt(point + hx) - distanceAt(point - hx)) / (h * 2);
	float dy = (distanceAt(point + hy) - distanceAt(point - hy)) / (h * 2);
	float dz = (distanceAt(point + hz) - distanceAt(point - hz)) / (h * 2);

	return vec3(dx, dy, dz);
}

vec3 normalAt(vec3 point) {
	return normalize(gradientAt(point));
}

Hit rayCast(vec3 origin, vec3 direction) {
	const float threshold = 0.001;
	vec3 pos = origin;
	float dist = 0;

	for (int i = 0; i < 1000; i++) {
		float d = distanceAt(pos);

		if (d < threshold) {
			vec3 normal = normalAt(pos);
			pos += normal*0.1;
			return Hit(true, pos, dist, normal, vec3(0.5));
		}

		pos += direction * d;
		dist += d;

		if (dist > 10) {
			break;
		}
	}

	return Hit(false, vec3(0), 0, vec3(0), vec3(0));
}

vec3 skyColor(vec3 direction) {
	// return vec3(1);
	float t = 0.5 * (direction.y + 1);
	return (1-t)*vec3(1, 1, 1) + t*vec3(0.5, 0.7, 1);
}

vec3 rayColor(vec3 origin, vec3 direction) {
	vec3 color = vec3(1);

	for (int i = 0; i < MAX_BOUNCE+1; i++) {
		Hit hit = rayCast(origin, direction);

		if (hit.hit) {
			color *= hit.color;
			origin = hit.pos;

			// diffuse
			vec3 target = hit.pos + hit.normal + sphericalRand();
			direction = normalize(target - hit.pos);

			// mirror
			// direction = reflect(direction, hit.normal);
		} else {
			color *= skyColor(direction);
			break;
		}
	}

	return color;
}

void main() {
	float screenPlaneSizeY = tan(radians(fov) / 2) * 2;
	float screenPlaneSizeX = screenPlaneSizeY * windowSize.x / windowSize.y;

	vec3 screenPlaneRight = vec3(1, 0, 0) * screenPlaneSizeX;
	vec3 screenPlaneUp = vec3(0, 1, 0) * screenPlaneSizeY;
	vec3 screenPlaneCenter = vec3(0, 0, -1);

	vec3 rayOrigin = vec3(0, 0, 0);
	rayOrigin = vec3(cameraMat * vec4(rayOrigin, 1));

	fragColor = vec3(0);

	for (int i = 0; i < SAMPLE_PER_FRAGMENT; i++) {
		vec2 sampleCoord = (fragCoord - 0.5) + (vec2(rand(), rand()) - 0.5) / windowSize;
		vec3 rayDirection = screenPlaneCenter + screenPlaneRight*sampleCoord.x + screenPlaneUp*sampleCoord.y;
		rayDirection = vec3(cameraMat * vec4(rayDirection, 0));

		fragColor += rayColor(rayOrigin, normalize(rayDirection));
	}

	fragColor /= SAMPLE_PER_FRAGMENT;

	// fragColor = vec3(rand());
}
