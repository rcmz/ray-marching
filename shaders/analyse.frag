#version 430

layout(binding=0) uniform sampler3D distanceSamp;
layout(binding=1) uniform sampler3D signSamp;
layout(binding=2) uniform sampler3D sdfSamp;
uniform ivec2 windowSize;
uniform int layer;
uniform int mode;
out vec3 fragColor;

vec3 distanceColor(float d) {
	d = clamp(d, -1, 1);
	d = (d + 1)/2;
	return mix(vec3(1, 0, 0), vec3(0, 0, 1), d);
}

void main() {
	vec2 fragCoord = gl_FragCoord.xy / windowSize;
	int layerMax = textureSize(sdfSamp, 0).z;
	float layerCoord = float(layer)/layerMax;
	vec3 coord = vec3(fragCoord, layerCoord);

	       if (mode == 0) { // distance
		fragColor = distanceColor(texture(distanceSamp, coord).r);
	} else if (mode == 1) { // sign
		fragColor = distanceColor(texture(signSamp, coord).r);
	} else if (mode == 2) { // sdf
		fragColor = distanceColor(texture(sdfSamp, coord).r);
	}
}
